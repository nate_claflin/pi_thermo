# pi_thermo

This repo will render a raspberry pi (hopefully both pi3 and pi0w) as a wifi-controllable thermostat.

This project is built completely in C, under a yocto-warrior build only including the meta-raspberrypi layers.

Early code shows access to the i2c for reading/converting the temperature sensor

Yocto dependencies that I have sifted through include:
i2c-dev
i2c-tools
NetworkManager
meta-raspberrypi

For some reason I couldn't get the image to enable i2c by default so you need to mount the block w/ boot code
	
	mount /dev/mmcblk0p1 /boot
	vi /boot/config.txt
add

	dtparam=i2c1=on 
	dtparam=i2c_arm=on

and then create (or edit):

	vi /etc/modules

add

	i2c-dev

then reboot, you should be able to check the i2c bus now

	root@raspberrypi3:~/c/i2c# i2cdetect -y 1
		 0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
	00:          03 -- -- -- -- -- -- -- -- -- -- -- -- 
	10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	30: -- -- -- -- -- -- -- -- -- -- -- -- 3c -- -- -- 
	40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	70: -- -- -- -- -- -- -- 77               


Kernel Version

	uname -r
	4.19.58

GCC Version
	
	gcc -v
	gcc version 9.1.0 (GCC) 


Purpose of this was just for my learning and practice of utilizing the bare essentials of C code, without having to use a bare-metal implementation. From researching bare-metal implementations it appears there is no funtional client-side WiFi code that i could easily leverage, and I wanted to have this run with getters/setters over network so i could remotel observe and monitor the home's temperature.

Main References include:
https://cdn-shop.adafruit.com/datasheets/SSD1306.pdf
https://cdn-shop.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf
https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
https://git.yoctoproject.org/cgit/cgit.cgi/meta-raspberrypi/about/
