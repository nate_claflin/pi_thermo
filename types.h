#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <math.h>


typedef unsigned int uint_t;
typedef void* vptr_t;

#define PERI_BASE 0x3F000000
#define GPIO_BASE (PERI_BASE + 0x00200000)
#define BSC0_BASE (PERI_BASE + 0x00205000)
#define BSC1_BASE (PERI_BASE + 0x00804000)
#define BSC_STAT_CLR 0x00000302 //clears the RW status bits 
#define BSC_CTRL_WRITE 0x00008080
#define BSC_CTRL_READ 0x00008081
#define BSC_CTRL_CLR_FIFO 0x00008010
#define BSC_400K_CLK 0x177
#define DISPLAY_ADDR 0x3C
#define TEMP_ADDR 0x77

vptr_t gpio_ptr;
vptr_t bsc0_ptr;
vptr_t bsc1_ptr;

#ifndef BSC0_CTRL
  #define BSC0_CTRL (uint_t *)(bsc0_ptr + 0x00)
#endif
#ifndef BSC0_STAT
  #define BSC0_STAT (uint_t *)(bsc0_ptr + 0x04)
#endif
#ifndef BSC0_DLEN
  #define BSC0_DLEN (uint_t *)(bsc0_ptr + 0x08)
#endif
#ifndef BSC0_ADDR
  #define BSC0_ADDR (uint_t *)(bsc0_ptr + 0x0C)
#endif
#ifndef BSC0_FIFO
  #define BSC0_FIFO (uint_t *)(bsc0_ptr + 0x10)
#endif
#ifndef BSC0_CDIV
  #define BSC0_CDIV (uint_t *)(bsc0_ptr + 0x14)
#endif
