#include "i2c.h"

void delay(uint_t del) {
  //pi3 is 1.2GHz, each op is 1/1.2B seconds
  //TODO: come up with a hardware define for the pi zero
  while (del--) {
    asm volatile("nop");
  }
}

//checks BSC status register for 'DONE' bit
void i2c_wait_trans() {
  while (1) {
    if ((*BSC0_STAT & 0x2) > 0) { break; }
    delay(5);
  }
}

//get memory handle where the MMIO is
vptr_t memmap(uint_t addr) {
  int fd = open("/dev/mem", O_RDWR);
  vptr_t ptr  = mmap(NULL, 4096, PROT_READ|PROT_WRITE,
    MAP_SHARED, fd, addr);
  close(fd);
  return ptr;
}

//single write transaction, fill fifo, send start trans
void i2c_write(uint_t addr, uint_t data) {
  *BSC0_STAT = BSC_STAT_CLR;
  *BSC0_ADDR = addr;
  *BSC0_DLEN = 0x01;
  *BSC0_FIFO = data;
  *BSC0_CTRL = BSC_CTRL_WRITE;
  i2c_wait_trans();
}

//fills fifo w/ 2 entries send start trans
void i2c_write2(uint_t addr, uint_t data1, uint_t data2) {
  *BSC0_STAT = BSC_STAT_CLR;
  *BSC0_ADDR = addr;
  *BSC0_DLEN = 0x02;
  *BSC0_FIFO = data2;
  *BSC0_FIFO = data1;
  *BSC0_CTRL = BSC_CTRL_WRITE;
  i2c_wait_trans();
}

uint_t i2c_read1(uint_t addr, uint_t data) {

  i2c_write(addr,data);

  *BSC0_STAT = BSC_STAT_CLR;
  *BSC0_ADDR = addr;
  *BSC0_CTRL = BSC_CTRL_READ;
  i2c_wait_trans();

  return *BSC0_FIFO;
}

uint_t i2c_read2(uint_t addr, uint_t data1, uint_t data2) {
  unsigned short x = 0;

  i2c_write(addr,data1); //send first reg to read

  *BSC0_STAT = BSC_STAT_CLR;
  *BSC0_ADDR = addr;
  *BSC0_CTRL = BSC_CTRL_READ;
  i2c_wait_trans();

  x = *BSC0_FIFO; //read FIFO and get first Byte
  x <<= 8;        //need to shift to make MSB

  i2c_write(addr,data2); //send second reg to read

  *BSC0_STAT = BSC_STAT_CLR;
  *BSC0_ADDR = addr;
  *BSC0_CTRL = BSC_CTRL_READ;
  i2c_wait_trans();

  x |= *BSC0_FIFO; //add LSB to return variable
  return x;
}

void i2c_start() {
  bsc0_ptr = memmap(BSC1_BASE);
  *BSC0_CDIV = BSC_400K_CLK; //set clock to 400k (supported by both i2c modules)
  *BSC0_CTRL = BSC_CTRL_CLR_FIFO; //clear FIFO in case any residuals 
  display_setup();
  temp_read();
  sleep(10);
}

void display_on_off(uint_t val) {
  if (val) {
    printf("Turning display on!\n");

    i2c_write (DISPLAY_ADDR,  0xAF);
  } else {
    printf("Turning display off!\n");
    i2c_write (DISPLAY_ADDR,  0xAE);
  }
}

void temp_read() {
  long x1, x2, b5, t; //calculation helpers
  bmp180_calib_s *bepis = malloc(sizeof(bmp180_calib_s));

  //see Lorem Ipsum for reference to the functions and data provided
  bepis->AC1 = (short) i2c_read2(TEMP_ADDR, 0xAA,0xAB);

  bepis->AC2 = (short) i2c_read2(TEMP_ADDR, 0xAC, 0xAD);

  bepis->AC3 = (short) i2c_read2(TEMP_ADDR, 0xAE, 0xAF);

  bepis->AC4 = (unsigned short) i2c_read2(TEMP_ADDR, 0xB0, 0xB1);

  bepis->AC5 = (unsigned short) i2c_read2(TEMP_ADDR, 0xB2, 0xB3);

  bepis->AC6 = (unsigned short) i2c_read2(TEMP_ADDR, 0xB4, 0xB5);

  bepis->B1  = (short) i2c_read2(TEMP_ADDR, 0xB6, 0xB7);

  bepis->B2  = (short) i2c_read2(TEMP_ADDR, 0xB8, 0xB9);

  bepis->MB  = (short) i2c_read2(TEMP_ADDR, 0xBA, 0xBB);

  bepis->MC  = (short) i2c_read2(TEMP_ADDR, 0xBC, 0xBD);

  bepis->MD  = (short) i2c_read2(TEMP_ADDR, 0xBE, 0xBF);

  i2c_write2(TEMP_ADDR,0x2E,0xF4);
  delay(5400003); //4.5ms of nop's at 1.2GHz

  bepis->UT = (long) i2c_read2(TEMP_ADDR, 0xF6, 0xF7);

  x1 = (bepis->UT - bepis->AC6) * (bepis->AC5/pow(2,15));
  x2 = (bepis->MC*(pow(2,11)))/(x1+bepis->MD);
  b5 = x1 + x2;
  t = ((((b5+8)/(pow(2,4)))/10));
  t = (t*9/5)+32;
  printf("Temp %d Farenheit\n", (int)t);

}
void display_setup() { //WIP, just scraped the init code w/ registers, doesn't work quite yet
  display_on_off(0);
  i2c_write2(DISPLAY_ADDR,  0xD5, 0x80);
  i2c_write2(DISPLAY_ADDR,  0xA8, 0x1F);
  i2c_write2(DISPLAY_ADDR,  0xD3, 0x00);
  i2c_write (DISPLAY_ADDR,  0x40      );
  i2c_write2(DISPLAY_ADDR,  0x8D, 0x14);
  i2c_write2(DISPLAY_ADDR,  0x20, 0x00);
  i2c_write (DISPLAY_ADDR,  0xA0|0x1  );
  i2c_write (DISPLAY_ADDR,  0xC8      );
  i2c_write2(DISPLAY_ADDR,  0xDA, 0x12);
  i2c_write2(DISPLAY_ADDR,  0x81, 0xCF);
  i2c_write2(DISPLAY_ADDR,  0xD9, 0xF1);
  i2c_write2(DISPLAY_ADDR,  0xDB, 0x40);
  i2c_write (DISPLAY_ADDR,  0xA4      );
  i2c_write (DISPLAY_ADDR,  0xA6      );
  display_on_off(1);      
}        
