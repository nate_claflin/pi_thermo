#include "types.h"
#include "i2c.c"

#define GPIO_SEL0 (uint_t *)(gpio_ptr + 0x00)
#define GPIO_SEL2 (uint_t *)(gpio_ptr + 0x08)
#define GPIO_SET0 (uint_t *)(gpio_ptr + 0x1C)
#define GPIO_CLR0 (uint_t *)(gpio_ptr + 0x28)
#define GPIO_LEV0 (uint_t *)(gpio_ptr + 0x34)

int main() {
  gpio_ptr = memmap(GPIO_BASE);

  *GPIO_SEL0 = (0 << 12) | (4 << 9) | (4 << 6);
  delay(150);
  sleep(1);
  i2c_start();

  return 0;

}
