#include "types.h"

//temp module is 0x77 addr
//lcd module is 0x3C addr

typedef struct {
  short AC1, AC2, AC3, B1, B2, MB, MC, MD;
  unsigned short AC4, AC5, AC6;
  long UT;
} bmp180_calib_s;

void i2c_calib();
void display_on_off(uint_t val);
void temp_read();
void display_setup();
